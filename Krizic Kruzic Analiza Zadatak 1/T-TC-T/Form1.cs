﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace T_TC_T
{
    public partial class Form1 : Form
    {
        bool potez = true;
        int potez_br = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void a_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (potez)
                b.Text = "X";
            else
                b.Text = "O";

            potez = !potez;
            b.Enabled = false;
            potez_br++;

            Pobjednik();
        }

        private void Pobjednik()
        {
            bool t_pobjednik = false;
            //x
            if((a1.Text == a2.Text)&&(a2.Text == a3.Text)&&(!a1.Enabled))
            {
                t_pobjednik = true;
            }
            else if ((a4.Text == a5.Text) && (a5.Text == a6.Text)&&(!a4.Enabled))
            {
                t_pobjednik = true;
            }
            else if ((a7.Text == a8.Text) && (a8.Text == a9.Text)&&(!a7.Enabled))
            {
                t_pobjednik = true;
            }

            //y
            if ((a1.Text == a4.Text) && (a4.Text == a7.Text) && (!a1.Enabled))
            {
                t_pobjednik = true;
            }
            else if ((a2.Text == a5.Text) && (a5.Text == a8.Text) && (!a2.Enabled))
            {
                t_pobjednik = true;
            }
            else if ((a3.Text == a6.Text) && (a6.Text == a9.Text) && (!a3.Enabled))
            {
                t_pobjednik = true;
            }

            //xy
            if ((a1.Text == a5.Text) && (a5.Text == a9.Text) && (!a1.Enabled))
            {
                t_pobjednik = true;
            }
            else if ((a3.Text == a5.Text) && (a5.Text == a7.Text) && (!a7.Enabled))
            {
                t_pobjednik = true;
            }

            if (t_pobjednik)
            {
                Onemoguci();
                String pobjednik = "";
                if (potez)
                    pobjednik = "O";
                else
                    pobjednik = "X";

                MessageBox.Show(pobjednik + " je pobjednik");
            }

            else
            {
                if (potez_br == 9)
                    MessageBox.Show("Nema pobjednika");
            }
        }
        private void Onemoguci()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch
            {

            }

        }

        private void Novaigra_Click(object sender, EventArgs e)
        {

            potez = true;
            potez_br = 0;
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                    b.Text = "";
                }
            }
            catch
            {

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
