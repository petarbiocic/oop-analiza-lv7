﻿namespace T_TC_T
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Novaigra = new System.Windows.Forms.Button();
            this.Izlaz = new System.Windows.Forms.Button();
            this.a1 = new System.Windows.Forms.Button();
            this.a2 = new System.Windows.Forms.Button();
            this.a3 = new System.Windows.Forms.Button();
            this.a6 = new System.Windows.Forms.Button();
            this.a5 = new System.Windows.Forms.Button();
            this.a4 = new System.Windows.Forms.Button();
            this.a9 = new System.Windows.Forms.Button();
            this.a8 = new System.Windows.Forms.Button();
            this.a7 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Novaigra
            // 
            this.Novaigra.Location = new System.Drawing.Point(13, 12);
            this.Novaigra.Name = "Novaigra";
            this.Novaigra.Size = new System.Drawing.Size(118, 35);
            this.Novaigra.TabIndex = 0;
            this.Novaigra.Text = "Nova Igra";
            this.Novaigra.UseVisualStyleBackColor = true;
            this.Novaigra.Click += new System.EventHandler(this.Novaigra_Click);
            // 
            // Izlaz
            // 
            this.Izlaz.Location = new System.Drawing.Point(137, 12);
            this.Izlaz.Name = "Izlaz";
            this.Izlaz.Size = new System.Drawing.Size(113, 35);
            this.Izlaz.TabIndex = 1;
            this.Izlaz.Text = "Izlaz";
            this.Izlaz.UseVisualStyleBackColor = true;
            this.Izlaz.Click += new System.EventHandler(this.Izlaz_Click);
            // 
            // a1
            // 
            this.a1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a1.Location = new System.Drawing.Point(13, 69);
            this.a1.Name = "a1";
            this.a1.Size = new System.Drawing.Size(75, 67);
            this.a1.TabIndex = 2;
            this.a1.UseVisualStyleBackColor = true;
            this.a1.Click += new System.EventHandler(this.a_Click);
            // 
            // a2
            // 
            this.a2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a2.Location = new System.Drawing.Point(94, 69);
            this.a2.Name = "a2";
            this.a2.Size = new System.Drawing.Size(75, 67);
            this.a2.TabIndex = 3;
            this.a2.UseVisualStyleBackColor = true;
            this.a2.Click += new System.EventHandler(this.a_Click);
            // 
            // a3
            // 
            this.a3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a3.Location = new System.Drawing.Point(175, 69);
            this.a3.Name = "a3";
            this.a3.Size = new System.Drawing.Size(75, 67);
            this.a3.TabIndex = 4;
            this.a3.UseVisualStyleBackColor = true;
            this.a3.Click += new System.EventHandler(this.a_Click);
            // 
            // a6
            // 
            this.a6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a6.Location = new System.Drawing.Point(175, 142);
            this.a6.Name = "a6";
            this.a6.Size = new System.Drawing.Size(75, 67);
            this.a6.TabIndex = 7;
            this.a6.UseVisualStyleBackColor = true;
            this.a6.Click += new System.EventHandler(this.a_Click);
            // 
            // a5
            // 
            this.a5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a5.Location = new System.Drawing.Point(94, 142);
            this.a5.Name = "a5";
            this.a5.Size = new System.Drawing.Size(75, 67);
            this.a5.TabIndex = 6;
            this.a5.UseVisualStyleBackColor = true;
            this.a5.Click += new System.EventHandler(this.a_Click);
            // 
            // a4
            // 
            this.a4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a4.Location = new System.Drawing.Point(13, 142);
            this.a4.Name = "a4";
            this.a4.Size = new System.Drawing.Size(75, 67);
            this.a4.TabIndex = 5;
            this.a4.UseVisualStyleBackColor = true;
            this.a4.Click += new System.EventHandler(this.a_Click);
            // 
            // a9
            // 
            this.a9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a9.Location = new System.Drawing.Point(174, 215);
            this.a9.Name = "a9";
            this.a9.Size = new System.Drawing.Size(75, 67);
            this.a9.TabIndex = 10;
            this.a9.UseVisualStyleBackColor = true;
            this.a9.Click += new System.EventHandler(this.a_Click);
            // 
            // a8
            // 
            this.a8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a8.Location = new System.Drawing.Point(93, 215);
            this.a8.Name = "a8";
            this.a8.Size = new System.Drawing.Size(75, 67);
            this.a8.TabIndex = 9;
            this.a8.UseVisualStyleBackColor = true;
            this.a8.Click += new System.EventHandler(this.a_Click);
            // 
            // a7
            // 
            this.a7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.a7.Location = new System.Drawing.Point(12, 215);
            this.a7.Name = "a7";
            this.a7.Size = new System.Drawing.Size(75, 67);
            this.a7.TabIndex = 8;
            this.a7.UseVisualStyleBackColor = true;
            this.a7.Click += new System.EventHandler(this.a_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 310);
            this.Controls.Add(this.a9);
            this.Controls.Add(this.a8);
            this.Controls.Add(this.a7);
            this.Controls.Add(this.a6);
            this.Controls.Add(this.a5);
            this.Controls.Add(this.a4);
            this.Controls.Add(this.a3);
            this.Controls.Add(this.a2);
            this.Controls.Add(this.a1);
            this.Controls.Add(this.Izlaz);
            this.Controls.Add(this.Novaigra);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Krizic Kruzic";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Click += new System.EventHandler(this.a_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Novaigra;
        private System.Windows.Forms.Button Izlaz;
        private System.Windows.Forms.Button a1;
        private System.Windows.Forms.Button a2;
        private System.Windows.Forms.Button a3;
        private System.Windows.Forms.Button a6;
        private System.Windows.Forms.Button a5;
        private System.Windows.Forms.Button a4;
        private System.Windows.Forms.Button a9;
        private System.Windows.Forms.Button a8;
        private System.Windows.Forms.Button a7;
    }
}

